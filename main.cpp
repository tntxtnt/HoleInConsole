#include <iostream>
#include <windows.h>

int main()
{
    HWND hWnd = GetConsoleWindow();

    if (hWnd)
    {
        SetWindowLong(hWnd, GWL_EXSTYLE, GetWindowLong(hWnd, GWL_EXSTYLE) | WS_EX_LAYERED);
        SetLayeredWindowAttributes(hWnd, RGB(255,255,0), 0, LWA_COLORKEY);
        HDC hdc = GetWindowDC(hWnd);
        if (hdc)
        {
            HBRUSH yellowBrush = (HBRUSH)CreateSolidBrush(RGB(255,255,0));
            HPEN redPen = (HPEN)CreatePen(PS_SOLID, 2, RGB(255,0,0));
            HBRUSH oldbrush = (HBRUSH)SelectObject(hdc, GetStockObject(NULL_BRUSH));
            SelectObject(hdc, yellowBrush);
            SelectObject(hdc, redPen);

            Ellipse(hdc,100,100,200,200);

            SelectObject(hdc, oldbrush);
        }
    }
    
    std::cin.get();
}
