## Hole in Console

Fun with old WinGDI: punch a hole through a console window.

#### Links
- [Daynhauhoc](https://daynhauhoc.com/t/su-dung-ky-thuat-gi-de-ve-mot-hinh-trong-suot-tren-man-hinh-cmd-voi-c/30749/4)
- [Stackoverflow](https://stackoverflow.com/questions/3970066/creating-a-transparent-window-in-c-win32)

![Screeshot 01](/md/ss01.png)